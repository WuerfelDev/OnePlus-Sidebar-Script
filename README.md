OnePlus-Sidebar-Script
======================

This is a sidebar script for the OnePlus forum.
It has its origins [here](https://github.com/kpsuperplane/OnePlus-Sidebar-Script). To the old [Forums Thread](https://forums.oneplus.net/threads/tool-oneplus-forum-sidebar-mod.208545/).



### Installation
Install tampermonkey for your browser https://www.tampermonkey.net/
<br>Then open https://gitlab.com/WuerfelDev/OnePlus-Sidebar-Script/-/raw/master/sidebar.user.js and click Install
<br>Done.

---

<!-- Update https://forums.oneplus.com/threads/tool-oneplus-forum-sidebar-mod.208545/page-19 -->
### Changelog
##### 2.9.6
Fixed updater, Removed mod tagging. Replaced Fan gathering category with community chats

##### 2.9.5
Removed unavailable emojis.

##### 2.9.4
With this version the source was moved to GitLab at https://gitlab.com/WuerfelDev/OnePlus-Sidebar-Script/

-----
##### Until 2.9.2
Old changelog: https://forums.oneplus.net/threads/tool-oneplus-forum-sidebar-mod.208545/#post-8332933
<br>Old GitHub: https://github.com/kpsuperplane/OnePlus-Sidebar-Script

---
<br><br>
Contributors: Mikasa Ackerman aka Kallen, Kevin Pei aka kp1234, Sam Prescott aka sp99, awkward_potato, WuerfelDev
